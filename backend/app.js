const WebSocket = require('ws');
const wss = new WebSocket.Server({port: 3000});

const constants = {
    messageTypes: {
        MOVE: 'move',
        PLAYER_INFORMATION: 'playerInformation',
        GAME_OVER: 'gameOver'
    },
};

let currentKey = 0;
let playerQueue = [];
let sessions = [];

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        let messageParts = message.split('|');
        let messageType = messageParts[0];
        let messageData = messageParts[1];
        switch (messageType) {
            case constants.messageTypes.MOVE: {
                let data = JSON.parse(messageData);
                let connectionKey = data.connectionKey;
                let move = data.move;
                sessions.forEach(session => {
                    if (session.player1.connectionKey === connectionKey) {
                        session.player1.connection.send(`${constants.messageTypes.MOVE}|${move}`);
                    } else if (session.player2.connectionKey === connectionKey) {
                        session.player2.connection.send(`${constants.messageTypes.MOVE}|${move}`);
                    }
                });
                break;
            }
            case constants.messageTypes.PLAYER_INFORMATION: {
                ws.id = currentKey++;
                let currentPlayer = {
                    name: messageData,
                    connectionKey: ws.id,
                    connection: ws,
                };
                if (playerQueue.length > 0) {
                    let opponent = playerQueue.pop();
                    let opponentClass = Math.random() > 0.5 ? 'light' : 'heavy';
                    let currentPlayerClass = opponentClass === 'light' ? 'heavy' : 'light';
                    let opponentMessageData = JSON.stringify({
                        localPlayerClass: opponentClass,
                        remotePlayerName: currentPlayer.name,
                        remotePlayerConnectionKey: currentPlayer.connectionKey,
                    });
                    let currentPlayerMessageData = JSON.stringify({
                        localPlayerClass: currentPlayerClass,
                        remotePlayerName: opponent.name,
                        remotePlayerConnectionKey: opponent.connectionKey,
                    });
                    opponent.connection.send(`${constants.messageTypes.PLAYER_INFORMATION}|${opponentMessageData}`);
                    currentPlayer.connection.send(`${constants.messageTypes.PLAYER_INFORMATION}|${currentPlayerMessageData}`);
                    sessions.push({
                        player1: opponent,
                        player2: currentPlayer
                    });
                } else {
                    playerQueue.push(currentPlayer);
                }
                break;
            }
            case constants.messageTypes.GAME_OVER: {
                let connectionKey = messageData;
                for (let i = 0; i < sessions.length; i++) {
                    let session = sessions[i];
                    if (session.player1.connectionKey === connectionKey || session.player2.connectionKey === connectionKey) {
                        sessions.splice(i, 1);
                        break;
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    });
});
