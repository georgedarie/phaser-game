const DEFAULT_ASSETS_PATH = 'assets/';
const GAME_WIDTH = 1500;
const GAME_HEIGHT = 650;
const BASE_DAMAGE = 0.0005;
const constants = {
    assets: {
        BACKGROUND: {
            name: 'background',
            path: `${DEFAULT_ASSETS_PATH}background/background.jpg`
        },
        HEAVY_BANDIT: {
            name: 'heavyBandit',
            path: `${DEFAULT_ASSETS_PATH}player/heavy.png`,
            frameWidth: 48,
            frameHeight: 50,
            scaleX: 3,
            scaleY: 4,
            actualSizeX: 26,
            actualSizeY: 40,
            positionX: 200,
            positionY: GAME_HEIGHT - 250,
            bounce: 0.05,
            facingLeft: false,
            animations: {
                idleLeft: {
                    key: 'heavyIdleLeft',
                    frames: {start: 0, end: 3},
                    frameRate: 5,
                    repeat: -1
                },
                idleRight: {
                    key: 'heavyIdleRight',
                    frames: {start: 52, end: 55},
                    frameRate: 5,
                    repeat: -1
                },
                walkLeft: {
                    key: 'heavyWalkLeft',
                    frames: {start: 15, end: 8},
                    frameRate: 10,
                    repeat: -1
                },
                walkRight: {
                    key: 'heavyWalkRight',
                    frames: {start: 40, end: 47},
                    frameRate: 10,
                    repeat: -1
                },
                attackLeft: {
                    key: 'heavyAttackLeft',
                    frames: {start: 20, end: 23},
                    frameRate: 10,
                    repeat: -1
                },
                attackRight: {
                    key: 'heavyAttackRight',
                    frames: {start: 51, end: 48},
                    frameRate: 10,
                    repeat: -1
                },
                damageFromLeft: {
                    key: 'heavyDamageFromLeft',
                    frames: {start: 32, end: 34},
                    frameRate: 1,
                    repeat: 0
                },
                damageFromRight: {
                    key: 'heavyDamageFromRight',
                    frames: {start: 39, end: 37},
                    frameRate: 1,
                    repeat: 0
                },
                deathFromLeft: {
                    key: 'heavyDeathFromLeft',
                    frames: {start: 32, end: 35},
                    frameRate: 5,
                    repeat: 0
                },
                deathFromRight: {
                    key: 'heavyDeathFromRight',
                    frames: {start: 39, end: 36},
                    frameRate: 5,
                    repeat: 0
                }
            }
        },
        LIGHT_BANDIT: {
            name: 'lightBandit',
            path: `${DEFAULT_ASSETS_PATH}player/light.png`,
            frameWidth: 48,
            frameHeight: 50,
            scaleX: 3,
            scaleY: 4,
            actualSizeX: 26,
            actualSizeY: 40,
            positionX: GAME_WIDTH - 200,
            positionY: GAME_HEIGHT - 250,
            bounce: 0.05,
            facingLeft: true,
            animations: {
                idleLeft: {
                    key: 'lightIdleLeft',
                    frames: {start: 0, end: 3},
                    frameRate: 5,
                    repeat: -1
                },
                idleRight: {
                    key: 'lightIdleRight',
                    frames: {start: 52, end: 55},
                    frameRate: 5,
                    repeat: -1
                },
                walkLeft: {
                    key: 'lightWalkLeft',
                    frames: {start: 15, end: 8},
                    frameRate: 10,
                    repeat: -1
                },
                walkRight: {
                    key: 'lightWalkRight',
                    frames: {start: 40, end: 47},
                    frameRate: 10,
                    repeat: -1
                },
                attackLeft: {
                    key: 'lightAttackLeft',
                    frames: {start: 20, end: 23},
                    frameRate: 10,
                    repeat: -1
                },
                attackRight: {
                    key: 'lightAttackRight',
                    frames: {start: 51, end: 48},
                    frameRate: 10,
                    repeat: -1
                },
                damageFromLeft: {
                    key: 'lightDamageFromLeft',
                    frames: {start: 32, end: 34},
                    frameRate: 1,
                    repeat: 0
                },
                damageFromRight: {
                    key: 'lightDamageFromRight',
                    frames: {start: 39, end: 37},
                    frameRate: 1,
                    repeat: 0
                },
                deathFromLeft: {
                    key: 'lightDeathFromLeft',
                    frames: {start: 32, end: 35},
                    frameRate: 5,
                    repeat: 0
                },
                deathFromRight: {
                    key: 'lightDeathFromRight',
                    frames: {start: 39, end: 36},
                    frameRate: 5,
                    repeat: 0
                }
            }
        },
        HEAVY_BANDIT_HEALTHBAR: {
            name: 'heavyBanditHealthBar',
            path: `${DEFAULT_ASSETS_PATH}player/heavy-healthbar.png`,
            positionX: 192,
            positionY: 32,
            scale: 0.5,
        },
        LIGHT_BANDIT_HEALTHBAR: {
            name: 'lightBanditHealthBar',
            path: `${DEFAULT_ASSETS_PATH}player/light-healthbar.png`,
            positionX: GAME_WIDTH - 192,
            positionY: 32,
            scale: 0.5,
        },
        HEAVY_BANDIT_HEALTH: {
            name: 'heavyBanditHealth',
            path: `${DEFAULT_ASSETS_PATH}player/heavy-health.png`,
            positionX: 125,
            positionY: 12,
            origin: 0,
            scale: 0.5,
        },
        LIGHT_BANDIT_HEALTH: {
            name: 'lightBanditHealth',
            path: `${DEFAULT_ASSETS_PATH}player/light-health.png`,
            positionX: GAME_WIDTH - 125,
            positionY: 52,
            origin: 1,
            scale: 0.5,
        },
        BOUNDING: {
            name: 'bounding',
            path: `${DEFAULT_ASSETS_PATH}background/bounding.png`,
            size: 64,
            scale: 2
        },
        FORM: {
            key: 'form',
            path: `${DEFAULT_ASSETS_PATH}form.html`,
            divId: 'form',
            eventName: 'click'
        }
    },
    PLAYER_NAMES_TEXT_PROPS: {
        fontFamily: 'Courier',
        fontSize: '30px',
        fontStyle: 'strong',
        heavyColor: '#3f48cc',
        lightColor: '#ffca18',
        stroke: '#000',
        strokeThickness: 5,
        heavyPositionX: 320,
        heavyPositionY: 14,
        lightPositionX: GAME_WIDTH - 320,
        lightPositionY: 48,
        lightOrigin: 1,
    },
    GAME_OVER_TEXT_PROPS: {
        fontFamily: 'Courier',
        fontSize: '80px',
        fontStyle: 'strong',
        color: '#f10',
        stroke: '#000',
        strokeThickness: 5,
        positionX: GAME_WIDTH / 2,
        positionY: GAME_HEIGHT / 2,
        origin: 0.5
    },
    REMOTE_PLAYER_MOVES: {
        WALK_LEFT: 'walkLeft',
        WALK_LEFT_AND_ATTACK: 'walkLeftAndAttack',
        WALK_RIGHT: 'walkRight',
        WALK_RIGHT_AND_ATTACK: 'walkRightAndAttack',
        ATTACK: 'attack',
        JUMP: 'jump'
    },
    WEBSOCKET: {
        url: '192.168.100.8',
        port: '3000',
        route: '/',
        messageTypes: {
            MOVE: 'move',
            PLAYER_INFORMATION: 'playerInformation',
            GAME_OVER: 'gameOver'
        }
    }
};
