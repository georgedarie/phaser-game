class WaitingRoom extends Phaser.Scene {
    constructor() {
        super('WaitingRoom');
    }

    preload() {
        this.load.html(constants.assets.FORM.key, constants.assets.FORM.path);
    }

    create() {
        this.cameras.main.setBackgroundColor('#1b1e21');
        let form = this.add.dom(GAME_WIDTH / 2, GAME_HEIGHT / 2).createFromCache(constants.assets.FORM.key, 'div');
        form.addListener(constants.assets.FORM.eventName);
        form.on(constants.assets.FORM.eventName, this.callback);
    }

    update() {
        if (isGameReady) {
            this.scene.start('FightingRoom');
        }
    }

    callback = function (event) {
        if (event.target.name === 'submitBtn') {
            let nameText = this.getChildByName('nameText');

            if (nameText.value !== '') {
                this.removeListener(constants.assets.FORM.eventName);
                this.getChildByName('submitBtn').style = 'display: none;';
                this.getChildByName('waitingText').style = '';
                localPlayerName = nameText.value;
                socket.send(`${constants.WEBSOCKET.messageTypes.PLAYER_INFORMATION}|${localPlayerName}`);
            }
        }
    }
}

class FightingRoom extends Phaser.Scene {
    constructor() {
        super('FightingRoom');
    }

    preload() {
        this.load.image(
            constants.assets.BACKGROUND.name,
            constants.assets.BACKGROUND.path
        );
        this.load.image(
            constants.assets.BOUNDING.name,
            constants.assets.BOUNDING.path
        );
        this.load.image(
            constants.assets.HEAVY_BANDIT_HEALTHBAR.name,
            constants.assets.HEAVY_BANDIT_HEALTHBAR.path
        );
        this.load.image(
            constants.assets.LIGHT_BANDIT_HEALTHBAR.name,
            constants.assets.LIGHT_BANDIT_HEALTHBAR.path
        );
        this.load.image(
            constants.assets.HEAVY_BANDIT_HEALTH.name,
            constants.assets.HEAVY_BANDIT_HEALTH.path
        );
        this.load.image(
            constants.assets.LIGHT_BANDIT_HEALTH.name,
            constants.assets.LIGHT_BANDIT_HEALTH.path
        );
        this.load.spritesheet(
            constants.assets.HEAVY_BANDIT.name,
            constants.assets.HEAVY_BANDIT.path,
            {
                frameWidth: constants.assets.HEAVY_BANDIT.frameWidth,
                frameHeight: constants.assets.HEAVY_BANDIT.frameHeight
            }
        );
        this.load.spritesheet(
            constants.assets.LIGHT_BANDIT.name,
            constants.assets.LIGHT_BANDIT.path,
            {
                frameWidth: constants.assets.LIGHT_BANDIT.frameWidth,
                frameHeight: constants.assets.LIGHT_BANDIT.frameHeight
            }
        );
    }

    create() {
        initializeDataFromBackend();
        initializeBackground(this);
        localPlayer = initializePlayer(this, localPlayerCharacterAsset, true);
        remotePlayer = initializePlayer(this, remotePlayerCharacterAsset, false);
        walls = initializeEnvironment(this, walls);
        initializeCollisions(this);
        localPlayer.health = initializeHealth(this, localPlayerHealthAsset);
        remotePlayer.health = initializeHealth(this, remotePlayerHealthAsset);
        initializeHealthBar(this, localPlayerHealthBarAsset);
        initializeHealthBar(this, remotePlayerHeathBarAsset);
        initializeText(this);
    }

    update() {
        if (!isGameOver) {
            moveLocalPlayer(
                this,
                localPlayerCharacterAsset.animations,
                remotePlayerCharacterAsset.animations
            );
            let remotePlayerMove = remotePlayerMovesQueue.length > 0 ? remotePlayerMovesQueue.pop() : '';
            moveRemotePlayer(
                this,
                remotePlayerCharacterAsset.animations,
                localPlayerCharacterAsset.animations,
                remotePlayerMove
            );
        } else {
            if (localPlayer.health.value <= 0) {
                playIdleAnimation(remotePlayer, remotePlayerCharacterAsset.animations);
            } else if (remotePlayer.health.value <= 0) {
                playIdleAnimation(localPlayer, localPlayerCharacterAsset.animations);
            }
            if(!sentGameOverEvent) {
                if (localPlayer.health.value <= 0) {
                    playDeathAnimation(this, localPlayer.gameObject, localPlayerCharacterAsset.animations);
                } else if (remotePlayer.health.value <= 0) {
                    playDeathAnimation(this, remotePlayer.gameObject, remotePlayerCharacterAsset.animations);
                }
                let gameOverText = this.add.text(
                    constants.GAME_OVER_TEXT_PROPS.positionX,
                    constants.GAME_OVER_TEXT_PROPS.positionY,
                    '        Game over!\n' +
                    'Press CTRL+R to play again.',
                    {
                        font: constants.GAME_OVER_TEXT_PROPS.fontSize,
                        fontStyle: constants.GAME_OVER_TEXT_PROPS.fontStyle,
                        stroke: constants.GAME_OVER_TEXT_PROPS.stroke,
                        strokeThickness: constants.GAME_OVER_TEXT_PROPS.strokeThickness,
                        color: constants.GAME_OVER_TEXT_PROPS.color,
                    }
                );
                gameOverText.setOrigin(constants.GAME_OVER_TEXT_PROPS.origin);
                sendGameOverMessageToWebsocket(remotePlayerInformation.connectionKey);
                sentGameOverEvent = true;
            }
        }
    }
}

const config = {
    type: Phaser.AUTO,
    width: GAME_WIDTH,
    height: GAME_HEIGHT,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 350}
        },
    },
    parent: constants.assets.FORM.divId,
    dom: {
        createContainer: true
    },
    scene: [WaitingRoom, FightingRoom]
};

let game = new Phaser.Game(config);
let players = [];
let localPlayer;
let remotePlayer;
let walls;
let isGameOver = false;
let localPlayerCharacterAsset;
let remotePlayerCharacterAsset;
let localPlayerHealthAsset;
let remotePlayerHealthAsset;
let localPlayerHealthBarAsset;
let remotePlayerHeathBarAsset;
let heavyPlayerName;
let lightPlayerName;
let localPlayerName = 'Player1';
let isGameReady = false;
let sentGameOverEvent = false;
let remotePlayerMovesQueue = [];
let localPlayerClass = 'light';
let remotePlayerInformation = {
    connectionKey: -1,
    name: 'Player2'
};
