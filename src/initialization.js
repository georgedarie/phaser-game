function initializeDataFromBackend() {
    if (localPlayerClass === 'heavy') {
        localPlayerCharacterAsset = constants.assets.HEAVY_BANDIT;
        remotePlayerCharacterAsset = constants.assets.LIGHT_BANDIT;
        localPlayerHealthAsset = constants.assets.HEAVY_BANDIT_HEALTH;
        remotePlayerHealthAsset = constants.assets.LIGHT_BANDIT_HEALTH;
        localPlayerHealthBarAsset = constants.assets.HEAVY_BANDIT_HEALTHBAR;
        remotePlayerHeathBarAsset = constants.assets.LIGHT_BANDIT_HEALTHBAR;
        heavyPlayerName = localPlayerName;
        lightPlayerName = remotePlayerInformation.name;
    } else {
        localPlayerCharacterAsset = constants.assets.LIGHT_BANDIT;
        remotePlayerCharacterAsset = constants.assets.HEAVY_BANDIT;
        localPlayerHealthAsset = constants.assets.LIGHT_BANDIT_HEALTH;
        remotePlayerHealthAsset = constants.assets.HEAVY_BANDIT_HEALTH;
        localPlayerHealthBarAsset = constants.assets.LIGHT_BANDIT_HEALTHBAR;
        remotePlayerHeathBarAsset = constants.assets.HEAVY_BANDIT_HEALTHBAR;
        heavyPlayerName = remotePlayerInformation.name;
        lightPlayerName = localPlayerName;
    }
}

function initializeBackground(game) {
    let background = game.add.image(GAME_WIDTH / 2, GAME_HEIGHT / 2, constants.assets.BACKGROUND.name);
    let backgroundScaleX = game.cameras.main.width / background.width;
    let backgroundScaleY = game.cameras.main.height / background.height;
    let backgroundScale = Math.max(backgroundScaleX, backgroundScaleY);
    background.setScale(backgroundScale).setScrollFactor(0);
}

function initializeEnvironment(game) {
    let walls = game.physics.add.staticGroup();
    const scale = constants.assets.BOUNDING.scale;
    const gameWidth = game.cameras.main.width;
    const gameHeight = game.cameras.main.height;
    for (let currentY = 32; currentY <= gameHeight; currentY += 64) {
        walls.create(32, currentY, constants.assets.BOUNDING.name).setScale(scale).refreshBody();
        walls.create(gameWidth - 32, currentY, constants.assets.BOUNDING.name).setScale(scale).refreshBody();
    }
    for (let currentX = 32; currentX <= gameWidth; currentX += 64) {
        walls.create(currentX, 32, constants.assets.BOUNDING.name).setScale(scale).refreshBody();
        walls.create(currentX, gameHeight - 32, constants.assets.BOUNDING.name).setScale(scale).refreshBody();
    }
    return walls;
}

function initializeCollisions(game) {
    players.forEach(player => {
        game.physics.add.collider(walls, player);
    });
}

function initializePlayer(game, playerAssetProperties, isLocalPlayer) {
    let currentPlayer = game.physics.add.sprite(
        playerAssetProperties.positionX,
        playerAssetProperties.positionY,
        playerAssetProperties.name
    );
    let player;
    if (isLocalPlayer) {
        player = {
            gameObject: currentPlayer,
            facingLeft: playerAssetProperties.facingLeft
        };
    } else {
        player = {
            gameObject: currentPlayer,
            facingLeft: playerAssetProperties.facingLeft
        }
    }
    players.push(currentPlayer);
    currentPlayer.setSize(playerAssetProperties.actualSizeX, playerAssetProperties.actualSizeY);
    currentPlayer.setBounce(playerAssetProperties.bounce);
    currentPlayer.setCollideWorldBounds(true);
    currentPlayer.setScale(playerAssetProperties.scaleX, playerAssetProperties.scaleY);

    for (let anim in playerAssetProperties.animations) {
        if (playerAssetProperties.animations.hasOwnProperty(anim)) {
            game.anims.create({
                key: playerAssetProperties.animations[anim].key,
                frames: game.anims.generateFrameNumbers(
                    playerAssetProperties.name,
                    playerAssetProperties.animations[anim].frames
                ),
                frameRate: playerAssetProperties.animations[anim].frameRate,
                repeat: playerAssetProperties.animations[anim].repeat
            });
        }
    }

    return player;
}

function initializeHealth(game, healthAssetProperties) {
    let health = game.add.image(
        healthAssetProperties.positionX,
        healthAssetProperties.positionY,
        healthAssetProperties.name
    );
    health.setOrigin(healthAssetProperties.origin);
    health.setScale(healthAssetProperties.scale);
    return {
        gameObject: health,
        value: 1
    }
}

function initializeHealthBar(game, healthBarAssetProperties) {
    let healthbar = game.add.image(
        healthBarAssetProperties.positionX,
        healthBarAssetProperties.positionY,
        healthBarAssetProperties.name
    );
    healthbar.setScale(healthBarAssetProperties.scale);
}

function initializeText(game) {
    game.add.text(
        constants.PLAYER_NAMES_TEXT_PROPS.heavyPositionX,
        constants.PLAYER_NAMES_TEXT_PROPS.heavyPositionY,
        heavyPlayerName,
        {
            font: constants.PLAYER_NAMES_TEXT_PROPS.fontSize,
            fontStyle: constants.PLAYER_NAMES_TEXT_PROPS.fontStyle,
            stroke: constants.PLAYER_NAMES_TEXT_PROPS.stroke,
            strokeThickness: constants.PLAYER_NAMES_TEXT_PROPS.strokeThickness,
            color: constants.PLAYER_NAMES_TEXT_PROPS.heavyColor
        }
    );
    let lightPlayerNameText = game.add.text(
        constants.PLAYER_NAMES_TEXT_PROPS.lightPositionX,
        constants.PLAYER_NAMES_TEXT_PROPS.lightPositionY,
        lightPlayerName,
        {
            font: constants.PLAYER_NAMES_TEXT_PROPS.fontSize,
            fontStyle: constants.PLAYER_NAMES_TEXT_PROPS.fontStyle,
            stroke: constants.PLAYER_NAMES_TEXT_PROPS.stroke,
            strokeThickness: constants.PLAYER_NAMES_TEXT_PROPS.strokeThickness,
            color: constants.PLAYER_NAMES_TEXT_PROPS.lightColor
        }
    );
    lightPlayerNameText.setOrigin(constants.PLAYER_NAMES_TEXT_PROPS.lightOrigin);
}
