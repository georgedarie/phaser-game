function moveLocalPlayer(game, localPlayerAnimationProperties, remotePlayerAnimationProperties) {
    let cursors = game.input.keyboard.createCursorKeys();
    let attackKey = game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.CTRL);
    let player = localPlayer.gameObject;
    if (localPlayer.health.value <= 0) {
        isGameOver = true;
        return;
    }
    if (cursors.left.isDown) {
        if (attackKey._justDown) {
            playAttackAnimation(
                game, localPlayer, remotePlayer, localPlayerAnimationProperties,
                remotePlayerAnimationProperties, 'left'
            );
            sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.WALK_LEFT_AND_ATTACK, remotePlayerInformation.connectionKey);
        } else {
            player.setVelocityX(-160);
            player.anims.play(localPlayerAnimationProperties.walkLeft.key, true);
            sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.WALK_LEFT, remotePlayerInformation.connectionKey);
        }
        localPlayer.facingLeft = true;
    } else if (cursors.right.isDown) {
        if (attackKey._justDown) {
            playAttackAnimation(
                game, localPlayer, remotePlayer, localPlayerAnimationProperties,
                remotePlayerAnimationProperties, 'right'
            );
            sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.WALK_RIGHT_AND_ATTACK, remotePlayerInformation.connectionKey);
        } else {
            player.setVelocityX(160);
            player.anims.play(localPlayerAnimationProperties.walkRight.key, true);
            sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.WALK_RIGHT, remotePlayerInformation.connectionKey);
        }
        localPlayer.facingLeft = false;
    } else if (attackKey._justDown) {
        if (localPlayer.facingLeft) {
            playAttackAnimation(
                game, localPlayer, remotePlayer, localPlayerAnimationProperties,
                remotePlayerAnimationProperties, 'left'
            );
        } else {
            playAttackAnimation(
                game, localPlayer, remotePlayer, localPlayerAnimationProperties,
                remotePlayerAnimationProperties, 'right'
            );
        }
        sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.ATTACK, remotePlayerInformation.connectionKey);
    } else {
        playIdleAnimation(localPlayer, localPlayerAnimationProperties);
    }
    if (cursors.up.isDown && player.body.touching.down) {
        player.setVelocityY(-200);
        sendPlayerMoveToWebsocket(constants.REMOTE_PLAYER_MOVES.JUMP, remotePlayerInformation.connectionKey);
    }
}

function moveRemotePlayer(game, remotePlayerAnimationProperties, localPlayerAnimationProperties, move) {
    let player = remotePlayer.gameObject;
    if (remotePlayer.health.value <= 0) {
        isGameOver = true;
        return;
    }
    if (move === constants.REMOTE_PLAYER_MOVES.WALK_LEFT ||
        move === constants.REMOTE_PLAYER_MOVES.WALK_LEFT_AND_ATTACK) {
        if (move === constants.REMOTE_PLAYER_MOVES.WALK_LEFT_AND_ATTACK) {
            playAttackAnimation(
                game, remotePlayer, localPlayer, remotePlayerAnimationProperties,
                localPlayerAnimationProperties, 'left'
            );
        } else {
            player.setVelocityX(-160);
            player.anims.play(remotePlayerAnimationProperties.walkLeft.key, true);
        }
        remotePlayer.facingLeft = true;
    } else if (move === constants.REMOTE_PLAYER_MOVES.WALK_RIGHT ||
        move === constants.REMOTE_PLAYER_MOVES.WALK_RIGHT_AND_ATTACK) {
        if (move === move === constants.REMOTE_PLAYER_MOVES.WALK_RIGHT_AND_ATTACK) {
            playAttackAnimation(
                game, remotePlayer, localPlayer, remotePlayerAnimationProperties,
                localPlayerAnimationProperties, 'right'
            );
        } else {
            player.setVelocityX(160);
            player.anims.play(remotePlayerAnimationProperties.walkRight.key, true);
        }
        remotePlayer.facingLeft = false;
    } else if (move === constants.REMOTE_PLAYER_MOVES.ATTACK) {
        if (remotePlayer.facingLeft) {
            playAttackAnimation(
                game, remotePlayer, localPlayer, remotePlayerAnimationProperties,
                localPlayerAnimationProperties, 'left'
            );
        } else {
            playAttackAnimation(
                game, remotePlayer, localPlayer, remotePlayerAnimationProperties,
                localPlayerAnimationProperties, 'right'
            );
        }
    } else {
        playIdleAnimation(remotePlayer, remotePlayerAnimationProperties);
    }
    if (move === constants.REMOTE_PLAYER_MOVES.JUMP) {
        player.setVelocityY(-200);
    }
}

function playIdleAnimation(player, playerAnimationProperties) {
    let playerGameObject = player.gameObject;
    playerGameObject.setVelocityX(0);
    if (player.facingLeft) {
        playerGameObject.anims.play(playerAnimationProperties.idleLeft.key, true);
    } else {
        playerGameObject.anims.play(playerAnimationProperties.idleRight.key, true);
    }
}

function playAttackAnimation(game, attackingPlayer, damagedPlayer, attackingPlayerAnimationProperties,
                             damagedPlayerAnimationProperties, direction) {
    let isDirectionLeft = direction === 'left';
    let attackAnimationKey = isDirectionLeft ?
        attackingPlayerAnimationProperties.attackLeft.key : attackingPlayerAnimationProperties.attackRight.key;
    let damageAnimationKey = isDirectionLeft ?
        damagedPlayerAnimationProperties.damageFromLeft.key : damagedPlayerAnimationProperties.damageFromRight.key;
    let attackingPlayerGameObject = attackingPlayer.gameObject;
    let damagedPlayerGameObject = damagedPlayer.gameObject;
    attackingPlayerGameObject.anims.play(attackAnimationKey, true);
    if (game.physics.world.collide(attackingPlayerGameObject, damagedPlayerGameObject)) {
        damagedPlayerGameObject.anims.play(damageAnimationKey);
        if (damagedPlayer.health.value > 0) {
            damagedPlayer.health.value -= BASE_DAMAGE;
            damagedPlayer.health.gameObject.scaleX = 0.5 * damagedPlayer.health.value;
        } else {
            playIdleAnimation(attackingPlayer, attackingPlayerAnimationProperties);
        }
    }
}

function playDeathAnimation(game, player, playerAnimationProperties) {
    player.anims.play(playerAnimationProperties.deathFromLeft.key);
}
