let socket = new WebSocket(`ws://${constants.WEBSOCKET.url}:${constants.WEBSOCKET.port}${constants.WEBSOCKET.route}`);

socket.onopen = function (e) {
    console.log(`Websocket successfully opened!\n${JSON.stringify(e)}`)
};

socket.onmessage = function (event) {
    let messageParts = event.data.split('|');
    let messageType = messageParts[0];
    let message = messageParts[1];
    switch (messageType) {
        case constants.WEBSOCKET.messageTypes.MOVE : {
            remotePlayerMovesQueue.push(message);
            break;
        }
        case constants.WEBSOCKET.messageTypes.PLAYER_INFORMATION : {
            let messageObject = JSON.parse(message);
            localPlayerClass = messageObject.localPlayerClass;
            remotePlayerInformation.name = messageObject.remotePlayerName;
            remotePlayerInformation.connectionKey = messageObject.remotePlayerConnectionKey;
            isGameReady = true;
            break;
        }
        default: {
            break;
        }
    }
};

socket.onerror = function (error) {
    console.log(`WEBSOCKET_ERROR: ${error.message}`);
};

function sendGameOverMessageToWebsocket(playerConnectionKey) {
    sendMessageToWebsocket(constants.WEBSOCKET.messageTypes.GAME_OVER, playerConnectionKey);
}

function sendPlayerMoveToWebsocket(moveKey, playerConnectionKey) {
    let message = {
        move: moveKey,
        connectionKey: playerConnectionKey
    };
    sendMessageToWebsocket(constants.WEBSOCKET.messageTypes.MOVE, JSON.stringify(message));
}

function sendMessageToWebsocket(messageType, message) {
    console.log();
    socket.send(`${messageType}|${message}`);
}
